import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path';

export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
  },
  server: {
    proxy: {
      '/api': {
        target: 'http://localhost:6969',
        ws: true,
        changeOrigin: true,
        rewrite: (apiPath) => apiPath.replace(/^\/api/, ''),
      },
    },
  },
});
