import React from 'react';
import {createBrowserRouter, createRoutesFromElements, Route, RouterProvider} from 'react-router-dom';
import AppLayout from "@/modules/AppLayout/AppLayout";
import LoginScreen from "@/modules/Authentication/LoginScreen";
import {LandingNavigator} from "@/modules/LandingNavigator/LandingNavigator";
import {RoleBoundary} from "@/core/components/RoleBoundary";
import {AdminRouter} from "@/modules/Admin/AdminRouter";
import NotFoundScreen from "@/core/components/NotFoundScreen";
import {ProfRouter} from "@/modules/Prof/ProfRouter";

const ADMIN_ROLES = ["ADMIN"];

const routes = createRoutesFromElements(
  <>
    <Route path="/" element={<AppLayout/>}>
      <Route index element={<LandingNavigator/>}/>
      <Route path="prof/*" element={<ProfRouter />} />
      <Route path="admin/*" element={
        <RoleBoundary permittedRoles={ADMIN_ROLES}>
          <AdminRouter/>
        </RoleBoundary>
      }/>
      <Route path="*" element={<NotFoundScreen />}/>
    </Route>
    <Route path="/login" element={<LoginScreen/>}/>
  </>,
);

const router = createBrowserRouter(routes);

export const Navigator: React.FC = () => (
  <RouterProvider router={router}/>
);
