import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  Stack,
  Button,
  Heading,
  useColorModeValue,
} from '@chakra-ui/react';
import {useLogin} from "@/modules/Authentication/query/AuthenticationQuery";
import {useCallback} from "react";
import {LoginPayload} from "@/modules/Authentication/query/AuthenticationTypes";
import {useForm} from "react-hook-form";
import {useNavigate} from "react-router-dom";

export default function LoginScreen() {
  const mutation = useLogin();

  const { register, handleSubmit } = useForm<LoginPayload>();

  const navigate = useNavigate();

  const onSubmit = useCallback((payload: LoginPayload) => {
    mutation.mutate(payload, {
      onSuccess: (token) => {
        localStorage.setItem("authToken", token.type.concat(" ").concat(token.accessToken));
        navigate('/')
      },
      onError: () => console.log('error!'),
    })
  }, [])


  return (
    <Flex
      minH="100vh"
      align="center"
      justify="center"
      bg="bg.normal"
    >
      <Stack spacing={8} mx="auto" maxW="lg" py={12} px={6} w="30%">
        <Stack align="center">
          <Heading fontSize="4xl">PCMan</Heading>
        </Stack>
        <Box
          rounded="lg"
          bg={useColorModeValue('white', 'gray.700')}
          boxShadow="lg"
          p={8}
          as="form"
          onSubmit={handleSubmit(onSubmit)}
        >
          <Stack spacing={4}>
            <FormControl id="username">
              <FormLabel>Nume de utilizator</FormLabel>
              <Input {...register('username')} type="username" />
            </FormControl>
            <FormControl id="password">
              <FormLabel>Parolă</FormLabel>
              <Input {...register('password')} type="password" />
            </FormControl>
            <Stack spacing={10}>
              <Button
                bg="mainButton"
                color="white"
                type="submit"
                _hover={{
                  bg: 'mainButtonHover',
                }}
              >
                Loghează-te
              </Button>
            </Stack>
          </Stack>
        </Box>
      </Stack>
    </Flex>
  );
}