import {create} from 'zustand';
import {CurrentUser} from "@/modules/Authentication/query/AuthenticationTypes";

interface BearState {
  currentUser: CurrentUser | null,
  setCurrentUser: (user: CurrentUser | null) => void
}

export const useCurrentUserStore = create<BearState>()((set) => ({
  currentUser: null,
  setCurrentUser: (currentUser: CurrentUser | null) => set({ currentUser }),
}));

export function useCurrentUser() {
  return useCurrentUserStore((state) => state.currentUser);
}

