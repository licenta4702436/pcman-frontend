import axios from "axios";
import {LoginPayload, AuthenticationToken} from "@/modules/Authentication/query/AuthenticationTypes";

export default ({
  login: (payload: LoginPayload) => axios.post<AuthenticationToken>('/public/auth/login', payload),
  logout: () => axios.post('/public/auth/logout'),
  getCurrentUser: () => axios.get('/public/auth/current'),
})