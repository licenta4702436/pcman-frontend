export interface CurrentUser {
  firstName: string;
  lastName: string;
  username: string;
  role: 'ADMIN' | 'PROF';
}

export interface LoginPayload {
  username: string;
  password: string;
}

export interface AuthenticationToken {
  accessToken: string,
  type: string
}