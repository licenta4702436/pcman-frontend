import api from './AuthenticationApi';
import {useMutation, useQuery, useQueryClient} from "@tanstack/react-query";
import {getData, invalidateQueriesOptions} from "@/core/query/QueryUtils";
import {AuthenticationToken, LoginPayload} from "@/modules/Authentication/query/AuthenticationTypes";

export function useLogin () {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("user", queryClient);
  return useMutation<AuthenticationToken, Error, LoginPayload>((payload) => api.login(payload).then(getData), options);
}

export function useLogout () {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("user", queryClient);
  return useMutation<unknown, Error, void>(() => {
      localStorage.clear();
      queryClient.clear();
      return api.logout().then(getData)
    }, options);
}

export function useCurrentUser(keySuffix?: string, options?: any) {
  return useQuery(['user', 'current', keySuffix], () => api.getCurrentUser().then(getData), options);
}