import React from "react";
import {Route, Routes} from "react-router-dom";
import {Layout} from "@/core/components/Layout";
import NotFoundScreen from "@/core/components/NotFoundScreen";
import {MaintenanceRouter} from "@/modules/Admin/Maintanence/MaintenanceRouter";
import {UserRouter} from "@/modules/Admin/User/UserRouter";
import {AdminDashboard} from "@/modules/Admin/AdminDashboard";


export const AdminRouter: React.FC = () => {
  return (
    <Routes>
      <Route path="/" element={<Layout />} >
        <Route index element={<AdminDashboard />} />
        <Route path="classroom/*" element={<MaintenanceRouter />} />
        <Route path="user/*" element={<UserRouter />} />
        <Route path="*" element={<NotFoundScreen /> } />
      </Route>
    </Routes>
  )
}