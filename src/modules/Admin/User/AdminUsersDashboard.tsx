import React from "react";
import {useNavigate} from "react-router-dom";
import {Screen} from "@/core/components/Screen";
import {Button, Card, CardBody, Flex, Text, useColorModeValue} from "@chakra-ui/react";

export const AdminUsersDashboard: React.FC = () => {
  const navigate = useNavigate();
  return (
    <Screen>
      <Button ml="50px" width="10%" onClick={() => navigate('../..')}>Înapoi</Button>
      <Flex flex={1}>
        <Card
          flex={1}
          m="50px"
          bg={useColorModeValue('white', 'gray.700')}
          boxShadow="base"
          rounded="xl"
        >
          <CardBody
            display="flex"
            justifyContent="center"
            alignItems="center"
            as="button"
            onClick={() => navigate('professor')}
          >
            <Text>Profesori</Text>
          </CardBody>
        </Card>
        <Card
          flex={1}
          m="50px"
          bg={useColorModeValue('white', 'gray.700')}
          boxShadow="base"
          rounded="xl"
        >
          <CardBody
            display="flex"
            justifyContent="center"
            alignItems="center"
            as="button"
            onClick={() => navigate('admin')}
          >
            <Text>Administratori</Text>
          </CardBody>
        </Card>
      </Flex>
    </Screen>
  )
}