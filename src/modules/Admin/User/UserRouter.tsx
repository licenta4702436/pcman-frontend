import React from "react";
import {Outlet, Route, Routes} from "react-router-dom";
import NotFoundScreen from "@/core/components/NotFoundScreen";
import {UserAdminRouter} from "@/modules/Admin/User/Admin/UserAdminRouter";
import {UserProfessorRouter} from "@/modules/Admin/User/Professor/UserProfessorRouter";
import {AdminUsersDashboard} from "@/modules/Admin/User/AdminUsersDashboard";


export const UserRouter: React.FC = () => {
  return (
    <Routes>
      <Route path="/" element={<Outlet/>}>
        <Route index element={<AdminUsersDashboard /> } />
        <Route path="admin/*" element={<UserAdminRouter />} />
        <Route path="professor/*" element={<UserProfessorRouter />} />
        <Route path="*" element={<NotFoundScreen/>}/>
      </Route>
    </Routes>
  )
}