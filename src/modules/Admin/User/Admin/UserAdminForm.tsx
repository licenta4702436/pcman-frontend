import {AdminMutation} from "@/modules/Admin/User/Admin/query/AdminTypes";
import React, {useEffect} from "react";
import {useForm} from "react-hook-form";
import {FormHandler, InputControl} from "@/core/components/Form";

export type UserAdminFormProps = {
  onSubmit: (values: AdminMutation) => void;
  isLoading?: boolean;
  defaultValues?: Partial<AdminMutation>;
  title: string;
};

export const UserAdminForm: React.FC<UserAdminFormProps> = (props) => {
  const {onSubmit, isLoading, defaultValues, title} = props;
  const {handleSubmit, control, reset} = useForm<AdminMutation>({
    defaultValues
  });

  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues]);

  return (
    <FormHandler onSubmit={handleSubmit(onSubmit)} isLoading={isLoading} title={title} confirmText="Salvează" discardText="Înapoi">
      <InputControl
        name="lastName"
        label="Nume"
        control={control}
      />
      <InputControl
        name="firstName"
        label="Prenume"
        control={control}
      />
      <InputControl
        name="username"
        label="Nume de utilizator"
        control={control}
      />
      <InputControl
        name="password"
        label="Parolă"
        control={control}
      />
    </FormHandler>
  );
}