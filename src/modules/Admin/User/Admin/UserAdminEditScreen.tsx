import React, {useCallback} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {Screen} from "@/core/components/Screen";
import {useEditAdmin, useGetOneAdmin} from "@/modules/Admin/User/Admin/query/AdminQuery";
import {AdminMutation} from "@/modules/Admin/User/Admin/query/AdminTypes";
import {UserAdminForm} from "@/modules/Admin/User/Admin/UserAdminForm";


export const UserAdminEditScreen: React.FC = () => {
  const {adminId} = useParams<'adminId'>();
  const {data: admin, isLoading, isError} = useGetOneAdmin(parseInt(adminId!));

  const mutation = useEditAdmin(parseInt(adminId!));

  const navigate = useNavigate();
  const onSubmit = useCallback((payload: AdminMutation) => {
    mutation.mutate(payload, {
      onSuccess: () => navigate('..'),
      onError: () => console.log("edit error!"),
    })
  }, [mutation]);

  return (
    <Screen isLoading={isLoading} isError={isError}>
      <UserAdminForm
        onSubmit={onSubmit}
        title="Administrator"
        defaultValues={admin}
      />
    </Screen>
  )
}