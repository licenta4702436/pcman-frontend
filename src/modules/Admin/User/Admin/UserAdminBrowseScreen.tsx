import {DataTable, TableConfig} from "@/core/components/DataTable/DataTable";
import React from "react";
import {useDeleteAdmin, useGetAllAdmins} from "@/modules/Admin/User/Admin/query/AdminQuery";

const tableConfig: TableConfig = {
  columns: [
    {
      key: "lastName",
      name: "Nume",
      clickable: true,
    },
    {
      key: "firstName",
      name: "Prenume",
    },
    {
      key: "username",
      name: "Nume de utilizator",
    }
  ],
};
export const UserAdminBrowseScreen: React.FC = () => {
  return (
    <DataTable config={tableConfig} useGetData={useGetAllAdmins} useDeleteData={useDeleteAdmin}/>
  )
}