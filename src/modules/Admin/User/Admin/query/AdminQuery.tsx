import {useMutation, useQuery, useQueryClient} from "@tanstack/react-query";
import api from './AdminApi';
import {getData, invalidateQueriesOptions} from "@/core/query/QueryUtils";
import {AdminMutation} from "@/modules/Admin/User/Admin/query/AdminTypes";

export function useGetAllAdmins() {
  return useQuery(['admin', 'all'], () => api.getAllAdmins().then(getData));
}

export function useGetOneAdmin(adminId: number) {
  return useQuery(['admin', 'one', adminId], () => api.getOneAdmin(adminId).then(getData));
}

export function useCreateAdmin() {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("admin", queryClient);
  return useMutation((payload: AdminMutation) => api.createAdmin(payload).then(getData), options);
}

export function useEditAdmin(adminId: number) {

  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("admin", queryClient);
  return useMutation((payload: AdminMutation) => api.editAdmin(adminId, payload).then(getData), options);
}

export function useDeleteAdmin() {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("admin", queryClient);
  return useMutation((adminId: number) => api.deleteAdmin(adminId).then(getData), options);
}