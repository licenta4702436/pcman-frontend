export interface Admin {
  id: number;
  firstName: string;
  lastName: string;
  userName: string;
}

export interface AdminMutation {
  firstName: string;
  lastName: string;
  userName: string;
  password: string;
}