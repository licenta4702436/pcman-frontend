import axios from "axios";
import {AdminMutation} from "@/modules/Admin/User/Admin/query/AdminTypes";

export default ({
  getAllAdmins: () => axios.get("/admin/admin"),
  getOneAdmin: (adminId: number) => axios.get(`/admin/admin/${adminId}`),
  createAdmin: (payload: AdminMutation) => axios.post("/admin/admin", payload),
  editAdmin: (adminId: number, payload: AdminMutation) => axios.put(`/admin/admin/${adminId}/edit`, payload),
  deleteAdmin: (adminId: number) => axios.delete(`/admin/admin/${adminId}`),
})