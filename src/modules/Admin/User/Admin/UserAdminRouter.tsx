import React from "react";
import {Outlet, Route, Routes} from "react-router-dom";
import {UserAdminBrowseScreen} from "@/modules/Admin/User/Admin/UserAdminBrowseScreen";
import {UserAdminEditScreen} from "@/modules/Admin/User/Admin/UserAdminEditScreen";
import {UserAdminCreateScreen} from "@/modules/Admin/User/Admin/UserAdminCreateScreen";
import NotFoundScreen from "@/core/components/NotFoundScreen";


export const UserAdminRouter: React.FC = () => {
  return (
    <Routes>
      <Route path="/" element={<Outlet/>} >
        <Route index element={<UserAdminBrowseScreen />} />
        <Route path=":adminId/edit" element={<UserAdminEditScreen />} />
        <Route path="create" element={<UserAdminCreateScreen /> } />
        <Route path="*" element={<NotFoundScreen/>}/>
      </Route>
    </Routes>
  );
}