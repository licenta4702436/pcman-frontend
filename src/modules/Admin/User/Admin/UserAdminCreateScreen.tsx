import React, {useCallback} from "react";
import {useNavigate} from "react-router-dom";
import {useCreateAdmin} from "@/modules/Admin/User/Admin/query/AdminQuery";
import {AdminMutation} from "@/modules/Admin/User/Admin/query/AdminTypes";
import {UserAdminForm} from "@/modules/Admin/User/Admin/UserAdminForm";


export const UserAdminCreateScreen: React.FC = () => {
  const mutation = useCreateAdmin();

  const navigate = useNavigate();
  const onSubmit = useCallback((payload: AdminMutation) => {
    mutation.mutate(payload, {
      onSuccess: () => navigate('..'),
      onError: () => console.log("edit error!"),
    })
  }, [mutation]);

  return (
    <UserAdminForm
      onSubmit={onSubmit}
      isLoading={mutation.isLoading}
      title="Administrator"
    />
  )
}