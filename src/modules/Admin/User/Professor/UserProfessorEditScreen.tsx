import React, {useCallback} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {Screen} from "@/core/components/Screen";
import {useEditProfessor, useGetOneProfessor} from "@/modules/Admin/User/Professor/query/ProfessorQuery";
import {ProfessorMutation} from "@/modules/Admin/User/Professor/query/ProfessorTypes";
import {UserProfessorForm} from "@/modules/Admin/User/Professor/UserProfessorForm";


export const UserProfessorEditScreen: React.FC = () => {
  const {professorId} = useParams<'professorId'>();
  const {data: professor, isLoading, isError} = useGetOneProfessor(parseInt(professorId!));

  const mutation = useEditProfessor(parseInt(professorId!));

  const navigate = useNavigate();
  const onSubmit = useCallback((payload: ProfessorMutation) => {
    mutation.mutate(payload, {
      onSuccess: () => navigate('..'),
      onError: () => console.log("edit error!"),
    })
  }, [mutation]);

  return (
    <Screen isLoading={isLoading} isError={isError}>
      <UserProfessorForm
        onSubmit={onSubmit}
        title="Profesor"
        defaultValues={professor}
      />
    </Screen>
  )
}