import React, {useCallback} from "react";
import {useNavigate} from "react-router-dom";
import {AdminMutation} from "@/modules/Admin/User/Admin/query/AdminTypes";
import {UserAdminForm} from "@/modules/Admin/User/Admin/UserAdminForm";
import {useCreateProfessor} from "@/modules/Admin/User/Professor/query/ProfessorQuery";
import {ProfessorMutation} from "@/modules/Admin/User/Professor/query/ProfessorTypes";


export const UserProfessorCreateScreen: React.FC = () => {
  const mutation = useCreateProfessor();

  const navigate = useNavigate();
  const onSubmit = useCallback((payload: ProfessorMutation) => {
    mutation.mutate(payload, {
      onSuccess: () => navigate('..'),
      onError: () => console.log("edit error!"),
    })
  }, [mutation]);

  return (
    <UserAdminForm
      onSubmit={onSubmit}
      isLoading={mutation.isLoading}
      title="Profesor"
    />
  )
}