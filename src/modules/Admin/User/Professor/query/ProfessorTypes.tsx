export interface Professor {
  id: number;
  firstName: string;
  lastName: string;
  userName: string;
}

export interface ProfessorMutation {
  firstName: string;
  lastName: string;
  userName: string;
  password: string;
}