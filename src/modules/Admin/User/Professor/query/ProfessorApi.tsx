import axios from "axios";
import {ProfessorMutation} from "@/modules/Admin/User/Professor/query/ProfessorTypes";

export default ({
  getAllProfessors: () => axios.get("/admin/professor"),
  getOneProfessor: (professorId: number) => axios.get(`/admin/professor/${professorId}`),
  createProfessor: (payload: ProfessorMutation) => axios.post("/admin/professor", payload),
  editProfessor: (professorId: number, payload: ProfessorMutation) => axios.put(`/admin/professor/${professorId}/edit`, payload),
  deleteProfessor: (professorId: number) => axios.delete(`/admin/professor/${professorId}`),
})