import {useMutation, useQuery, useQueryClient} from "@tanstack/react-query";
import api from './ProfessorApi';
import {getData, invalidateQueriesOptions} from "@/core/query/QueryUtils";
import {ProfessorMutation} from "@/modules/Admin/User/Professor/query/ProfessorTypes";

export function useGetAllProfessors() {
  return useQuery(['professor', 'all'], () => api.getAllProfessors().then(getData));
}

export function useGetOneProfessor(professorId: number) {
  return useQuery(['professor', 'one', professorId], () => api.getOneProfessor(professorId).then(getData));
}

export function useCreateProfessor() {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("professor", queryClient);
  return useMutation((payload: ProfessorMutation) => api.createProfessor(payload).then(getData), options);
}

export function useEditProfessor(professorId: number) {

  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("professor", queryClient);
  return useMutation((payload: ProfessorMutation) => api.editProfessor(professorId, payload).then(getData), options);
}

export function useDeleteProfessor() {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("professor", queryClient);
  return useMutation((professorId: number) => api.deleteProfessor(professorId).then(getData), options);
}