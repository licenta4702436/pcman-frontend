import {DataTable, TableConfig} from "@/core/components/DataTable/DataTable";
import React from "react";
import {useDeleteProfessor, useGetAllProfessors} from "@/modules/Admin/User/Professor/query/ProfessorQuery";

const tableConfig: TableConfig = {
  columns: [
    {
      key: "lastName",
      name: "Nume",
      clickable: true,
    },
    {
      key: "firstName",
      name: "Prenume",
    },
    {
      key: "username",
      name: "Nume de utilizator",
    }
  ],
};
export const UserProfessorBrowseScreen: React.FC = () => {
  return (
    <DataTable config={tableConfig} useGetData={useGetAllProfessors} useDeleteData={useDeleteProfessor}/>
  )
}