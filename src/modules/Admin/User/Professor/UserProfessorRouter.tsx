import React from "react";
import {Outlet, Route, Routes} from "react-router-dom";
import {UserProfessorBrowseScreen} from "@/modules/Admin/User/Professor/UserProfessorBrowseScreen";
import NotFoundScreen from "@/core/components/NotFoundScreen";
import {UserProfessorEditScreen} from "@/modules/Admin/User/Professor/UserProfessorEditScreen";
import {UserProfessorCreateScreen} from "@/modules/Admin/User/Professor/UserProfessorCreateScreen";


export const UserProfessorRouter: React.FC = () => {
  return (
    <Routes>
      <Route path="/" element={<Outlet/>} >
        <Route index element={<UserProfessorBrowseScreen />} />
        <Route path=":professorId/edit" element={<UserProfessorEditScreen />} />
        <Route path="create" element={<UserProfessorCreateScreen />} />
        <Route path="*" element={<NotFoundScreen/>}/>
      </Route>
    </Routes>
  );
}