import React, {useEffect} from "react";
import {useForm} from "react-hook-form";
import {FormHandler, InputControl} from "@/core/components/Form";
import {ProfessorMutation} from "@/modules/Admin/User/Professor/query/ProfessorTypes";

export type UserProfessorFormProps = {
  onSubmit: (values: ProfessorMutation) => void;
  isLoading?: boolean;
  defaultValues?: Partial<ProfessorMutation>;
  title: string;
};

export const UserProfessorForm: React.FC<UserProfessorFormProps> = (props) => {
  const {onSubmit, isLoading, defaultValues, title} = props;
  const {handleSubmit, control, reset} = useForm<ProfessorMutation>({
    defaultValues
  });

  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues]);

  return (
    <FormHandler onSubmit={handleSubmit(onSubmit)} isLoading={isLoading} title={title} confirmText="Salvează" discardText="Înapoi">
      <InputControl
        name="lastName"
        label="Nume"
        control={control}
      />
      <InputControl
        name="firstName"
        label="Prenume"
        control={control}
      />
      <InputControl
        name="username"
        label="Nume de utilizator"
        control={control}
      />
      <InputControl
        name="password"
        label="Parolă"
        control={control}
      />
    </FormHandler>
  );
}