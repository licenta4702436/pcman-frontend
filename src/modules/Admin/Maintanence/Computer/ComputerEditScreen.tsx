import React, {useCallback} from "react";
import {useEditComputer, useGetOneComputer} from "@/modules/Admin/Maintanence/Computer/query/ComputerQuery";
import {useNavigate, useParams} from "react-router-dom";
import {ComputerMutation} from "@/modules/Admin/Maintanence/Computer/query/ComputerTypes";
import {ComputerForm} from "@/modules/Admin/Maintanence/Computer/ComputerForm";
import {Screen} from "@/core/components/Screen";

export const ComputerEditScreen: React.FC = () => {
  const {computerId} = useParams<'computerId'>();
  const {data: computer, isLoading, isError} = useGetOneComputer(parseInt(computerId!));

  const mutation = useEditComputer(parseInt(computerId!));

  const navigate = useNavigate();
  const onSubmit = useCallback((payload: ComputerMutation) => {
    mutation.mutate(payload, {
      onSuccess: () => navigate('..'),
      onError: () => console.log("edit error!"),
    })
  }, [mutation]);

  return (
    <Screen isLoading={isLoading} isError={isError}>
      <ComputerForm
        onSubmit={onSubmit}
        title="Calculator"
        defaultValues={computer}
      />
    </Screen>
  )
}
