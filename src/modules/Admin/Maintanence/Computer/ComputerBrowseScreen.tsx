import {DataTable, TableConfig} from "@/core/components/DataTable/DataTable";
import React from "react";
import {
  useDeleteComputer,
  useGetAllComputerOfClassroom
} from "@/modules/Admin/Maintanence/Computer/query/ComputerQuery";

const tableConfig: TableConfig = {
  columns: [
    {
      key: "name",
      name: "Nume",
      clickable: true,
    },
    {
      key: "ipAddress",
      name: "Adresă IP",
    },
    {
      key: "alias",
      name: "Alias",
    },
  ],
};

export type ComputerBrowseScreenProps = {
  classroomId: number;
}

export const ComputerBrowseScreen: React.FC<ComputerBrowseScreenProps> = (props) => {
  const {classroomId} = props;
  return (
    <DataTable config={tableConfig} useGetData={useGetAllComputerOfClassroom} useDeleteData={useDeleteComputer} dependencies={[classroomId]} extendUrl/>
  )
}