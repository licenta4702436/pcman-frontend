import {ComputerMutation} from "@/modules/Admin/Maintanence/Computer/query/ComputerTypes";
import React, {useEffect} from "react";
import {useForm} from "react-hook-form";
import {FormHandler, InputControl} from "@/core/components/Form";
import {useParams} from "react-router-dom";

export type ComputerFormProps = {
  onSubmit: (values: ComputerMutation) => void;
  isLoading?: boolean;
  defaultValues?: Partial<ComputerMutation>;
  title: string;
};

export const ComputerForm: React.FC<ComputerFormProps> = (props) => {
  const {classroomId} = useParams<'classroomId'>();
  const {onSubmit, isLoading, defaultValues, title} = props;
  const {handleSubmit, control, reset, setValue} = useForm<ComputerMutation>({
    defaultValues
  });

  useEffect(() => {
    reset(defaultValues);
    if (!defaultValues) {
      setValue('status', 'ON');
      setValue('classroomId', parseInt(classroomId!));
    }
  }, [classroomId, defaultValues]);

  return (
    <FormHandler onSubmit={handleSubmit(onSubmit)} isLoading={isLoading} title={title} confirmText="Salvează" discardText="Înapoi">
      <InputControl
        name="name"
        label="Nume"
        control={control}
      />
      <InputControl
        name="ipAddress"
        label="Adresă IP"
        control={control}
      />
      <InputControl
        name="alias"
        label="Alias"
        control={control}
      />
    </FormHandler>
  )
}