import React, {useCallback} from "react";
import {useNavigate} from "react-router-dom";
import {ComputerForm} from "@/modules/Admin/Maintanence/Computer/ComputerForm";
import {useCreateComputer} from "@/modules/Admin/Maintanence/Computer/query/ComputerQuery";
import {ComputerMutation} from "@/modules/Admin/Maintanence/Computer/query/ComputerTypes";


export const ComputerCreateScreen: React.FC = () => {
  const mutation = useCreateComputer();

  const navigate = useNavigate();
  const onSubmit = useCallback((payload: ComputerMutation) => {
    mutation.mutate(payload, {
      onSuccess: () => navigate('..'),
      onError: () => console.log("edit error!"),
    })
  }, [mutation]);

  return (
    <ComputerForm
      onSubmit={onSubmit}
      isLoading={mutation.isLoading}
      title="Calculator"
    />
  )
}