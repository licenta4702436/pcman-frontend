import axios from "axios";
import {ComputerMutation} from "@/modules/Admin/Maintanence/Computer/query/ComputerTypes";


export default ({
  getAllComputersOfClassroom: (classroomId: number) => axios.get(`/admin/computer/classroom/${classroomId}`),
  getOneComputer: (computerId: number) => axios.get(`/admin/computer/${computerId}`),
  createComputer: (payload: ComputerMutation) => axios.post("/admin/computer", payload),
  editComputer: (computerId: number, payload: ComputerMutation) => axios.put(`/admin/computer/${computerId}/edit`, payload),
  deleteComputer: (computerId: number) => axios.delete(`/admin/computer/${computerId}`),
})