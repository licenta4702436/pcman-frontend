export interface Computer {
  id: number;
  name: string;
  ipAddress: string;
  alias: string;
  status: {
    label: string;
    value: string;
  };
}

export interface ComputerMutation {
  name: string;
  ipAddress: string;
  alias: string;
  status: string;
  classroomId: number;
}

export interface ComputerCommandMutation {
  command: string;
}

export interface CreateFilePayload {
  path: string;
}

export interface DeleteFilePayload {
  path: string;
}