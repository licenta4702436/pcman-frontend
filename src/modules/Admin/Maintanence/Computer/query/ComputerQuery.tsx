import {useMutation, useQuery, useQueryClient} from "@tanstack/react-query";
import api from "./ComputerApi";
import {getData, invalidateQueriesOptions} from "@/core/query/QueryUtils";
import {ComputerMutation} from "@/modules/Admin/Maintanence/Computer/query/ComputerTypes";


export function useGetAllComputerOfClassroom(classroomId: number) {
  return useQuery(['computer', 'all', 'classroom'], () => api.getAllComputersOfClassroom(classroomId).then(getData));
}

export function useGetOneComputer(computerId: number) {
  return useQuery(['computer', 'one', computerId], () => api.getOneComputer(computerId).then(getData));
}

export function useCreateComputer() {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("computer", queryClient);
  return useMutation((payload: ComputerMutation) => api.createComputer(payload).then(getData), options);
}

export function useEditComputer(computerId: number) {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("computer", queryClient);
  return useMutation((payload: ComputerMutation) => api.editComputer(computerId, payload).then(getData), options);
}

export function useDeleteComputer() {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("computer", queryClient);
  return useMutation((computerId: number) => api.deleteComputer(computerId).then(getData), options);
}