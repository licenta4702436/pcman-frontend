import React from "react";
import {useParams} from "react-router-dom";
import {useGetOneClassroom} from "@/modules/Admin/Maintanence/Classroom/query/ClassroomQuery";
import {Screen} from "@/core/components/Screen";
import {Tab, TabList, TabPanel, TabPanels, Tabs} from "@chakra-ui/react";
import {ClassroomEditDetails} from "@/modules/Admin/Maintanence/Classroom/ClassroomEditDetails";
import {ComputerBrowseScreen} from "@/modules/Admin/Maintanence/Computer/ComputerBrowseScreen";

export const ClassroomEditScreen: React.FC = () => {
  const {classroomId} = useParams<'classroomId'>();
  const {data, isLoading, isError} = useGetOneClassroom(parseInt(classroomId!));

  return (
    <Screen p="0px" isLoading={isLoading} isError={isError}>
      <Tabs isLazy>
        <TabList bg="white">
          <Tab>Editează</Tab>
          <Tab>Calculatoare</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <ClassroomEditDetails classroom={data} classroomId={parseInt(classroomId!)} />
          </TabPanel>
          <TabPanel>
            <ComputerBrowseScreen classroomId={parseInt(classroomId!)} />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Screen>
  )
}