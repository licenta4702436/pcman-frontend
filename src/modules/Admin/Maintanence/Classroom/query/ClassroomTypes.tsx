import {Computer} from "@/modules/Admin/Maintanence/Computer/query/ComputerTypes";


export interface Classroom {
  id: number;
  name: string;
  computers: Computer[];
}

export interface ClassroomMutation {
  name: string;
  professor: {
    id: number;
    title: string;
  };
}

