import api from './ClassroomApi';
import {useMutation, useQuery, useQueryClient} from "@tanstack/react-query";
import {getData, invalidateQueriesOptions} from "@/core/query/QueryUtils";
import {ClassroomMutation} from "@/modules/Admin/Maintanence/Classroom/query/ClassroomTypes";

export function useGetAllClassrooms() {
  return useQuery(['classroom', 'all'], () => api.getAllClassrooms().then(getData));
}

export function useGetOneClassroom(classroomId: number) {
  return useQuery(['classroom', 'one', classroomId], () => api.getOneClassroom(classroomId).then(getData));
}

export function useCreateClassroom() {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("classroom", queryClient);
  return useMutation((payload: ClassroomMutation) => api.createClassroom(payload).then(getData), options);
}

export function useEditClassroom(classroomId: number) {

  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("classroom", queryClient);
  return useMutation((payload: ClassroomMutation) => api.editClassroom(classroomId, payload).then(getData), options);
}

export function useDeleteClassroom() {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("classroom", queryClient);
  return useMutation((classroomId: number) => api.deleteClassroom(classroomId).then(getData), options);
}
