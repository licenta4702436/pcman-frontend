import axios from "axios";
import {ClassroomMutation} from "@/modules/Admin/Maintanence/Classroom/query/ClassroomTypes";

export default ({
  getAllClassrooms: () => axios.get("/admin/classroom"),
  getOneClassroom: (classroomId: number) => axios.get(`/admin/classroom/${classroomId}`),
  createClassroom: (payload: ClassroomMutation) => axios.post("/admin/classroom", payload),
  editClassroom: (classroomId: number, payload: ClassroomMutation) => axios.put(`/admin/classroom/${classroomId}/edit`, payload),
  deleteClassroom: (classroomId: number) => axios.delete(`/admin/classroom/${classroomId}`),
})