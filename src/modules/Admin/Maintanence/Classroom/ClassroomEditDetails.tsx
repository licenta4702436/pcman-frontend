import React, {useCallback} from "react";
import {ClassroomForm} from "@/modules/Admin/Maintanence/Classroom/ClassroomForm";
import {useEditClassroom} from "@/modules/Admin/Maintanence/Classroom/query/ClassroomQuery";
import {ClassroomMutation} from "@/modules/Admin/Maintanence/Classroom/query/ClassroomTypes";
import {useNavigate} from "react-router-dom";


export type ClassroomEditDetailsProps = {
  classroom: any;
  classroomId: number;
}

export const ClassroomEditDetails: React.FC<ClassroomEditDetailsProps> = (props) => {
  const {classroom, classroomId} = props;
  const mutation = useEditClassroom(classroomId);

  const navigate = useNavigate();
  const onSubmit = useCallback((payload: ClassroomMutation) => {
    mutation.mutate(payload, {
      onSuccess: () => navigate('..'),
      onError: () => console.log("edit error!"),
    })
  }, [mutation]);

  return (
    <ClassroomForm
      onSubmit={onSubmit}
      isLoading={mutation.isLoading}
      defaultValues={classroom}
      title="Laborator"
    />
  )
}