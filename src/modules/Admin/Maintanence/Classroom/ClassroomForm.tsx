import React, {useEffect, useMemo} from "react";
import {FormHandler, InputControl} from "@/core/components/Form";
import {useForm} from "react-hook-form";
import {ClassroomMutation} from "@/modules/Admin/Maintanence/Classroom/query/ClassroomTypes";
import {SelectControl} from "@/core/components/Form/SelectControl";
import {useGetAllProfessors} from "@/modules/Admin/User/Professor/query/ProfessorQuery";


export type ClassroomFormProps = {
  onSubmit: (values: ClassroomMutation) => void;
  isLoading?: boolean;
  defaultValues?: Partial<ClassroomMutation> | undefined;
  title: string;
};

const selectProps = {
  isSearchable: true,
  isClearable: true,
  getOptionLabel: (option: any) => option.title,
  getOptionValue: (option: any) => option.id,
};

export const ClassroomForm: React.FC<ClassroomFormProps> = (props) => {
  const {onSubmit, isLoading, defaultValues, title} = props;
  const {handleSubmit, control, reset} = useForm<ClassroomMutation>({
    defaultValues
  });

  const {data: professors, isLoading: professorsIsLoading, isError: professorsIsError} = useGetAllProfessors();
  const professorsOptions = useMemo(() => (professors ?? []).map((professor) => ({ title: professor.lastName.concat(" ").concat(professor.firstName), id: professor.id })), [professors]);

  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues]);

  return (
    <FormHandler onSubmit={handleSubmit(onSubmit)} isLoading={isLoading} title={title} confirmText="Salvează" discardText="Înapoi">
      <InputControl
        name="name"
        label="Nume"
        control={control}
      />
      <SelectControl
        name="professor"
        label="Profesor"
        control={control}
        options={professorsOptions}
        disabled={professorsIsLoading || professorsIsError}
        select={selectProps}
      />
    </FormHandler>
  )
}