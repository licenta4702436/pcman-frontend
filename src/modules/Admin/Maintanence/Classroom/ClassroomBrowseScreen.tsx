import React from "react";
import {useDeleteClassroom, useGetAllClassrooms} from "@/modules/Admin/Maintanence/Classroom/query/ClassroomQuery";
import {DataTable, TableConfig} from "@/core/components/DataTable/DataTable";

const tableConfig: TableConfig = {
  columns: [
    {
      key: "name",
      name: "Nume",
      clickable: true,
    },
  ],
};
export const ClassroomBrowseScreen: React.FC = () => {
  return (
    <DataTable config={tableConfig} useGetData={useGetAllClassrooms} useDeleteData={useDeleteClassroom}/>
  )
}