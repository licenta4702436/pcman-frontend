import React, {useCallback} from "react";
import {ClassroomForm} from "@/modules/Admin/Maintanence/Classroom/ClassroomForm";
import {useCreateClassroom} from "@/modules/Admin/Maintanence/Classroom/query/ClassroomQuery";
import {ClassroomMutation} from "@/modules/Admin/Maintanence/Classroom/query/ClassroomTypes";
import {useNavigate} from "react-router-dom";


export const ClassroomCreateScreen: React.FC = () => {
  const mutation = useCreateClassroom();

  const navigate = useNavigate();
  const onSubmit = useCallback((payload: ClassroomMutation) => {
    mutation.mutate(payload, {
      onSuccess: () => navigate('..'),
      onError: () => console.log("edit error!"),
    })
  }, [mutation]);
  return (
    <ClassroomForm
     onSubmit={onSubmit}
     isLoading={mutation.isLoading}
     title="Create"
    />
  )
}