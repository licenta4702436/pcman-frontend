import React from "react";
import {Outlet, Route, Routes} from "react-router-dom";
import NotFoundScreen from "@/core/components/NotFoundScreen";
import {ClassroomBrowseScreen} from "@/modules/Admin/Maintanence/Classroom/ClassroomBrowseScreen";
import {ClassroomEditScreen} from "@/modules/Admin/Maintanence/Classroom/ClassroomEditScreen";
import {ComputerEditScreen} from "@/modules/Admin/Maintanence/Computer/ComputerEditScreen";
import {ClassroomCreateScreen} from "@/modules/Admin/Maintanence/Classroom/ClassroomCreateScreen";
import {ComputerCreateScreen} from "@/modules/Admin/Maintanence/Computer/ComputerCreateScreen";


export const MaintenanceRouter: React.FC = () => {
  return (
    <Routes>
      <Route path="/" element={<Outlet/>}>
        <Route index element={<ClassroomBrowseScreen/>}/>
        <Route path="create" element={<ClassroomCreateScreen />} />
        <Route path=":classroomId/edit" element={<ClassroomEditScreen />} />
        <Route path=":classroomId/create" element={<ComputerCreateScreen />} />
        <Route path=":classroomId/:computerId/edit" element={<ComputerEditScreen />} />
        <Route path="*" element={<NotFoundScreen/>}/>
      </Route>
    </Routes>
  )
}