import React from "react";
import {Button, Card, CardBody, Flex, Text, useColorModeValue} from "@chakra-ui/react";
import {Screen} from "@/core/components/Screen";
import {useNavigate} from "react-router-dom";

export const AdminDashboard: React.FC = () => {
  const navigate = useNavigate();
  return (
    <Screen>
      <Flex flex={1}>
        <Card
          flex={1}
          m="50px"
          bg={useColorModeValue('white', 'gray.700')}
          boxShadow="base"
          rounded="xl"
        >
          <CardBody
            display="flex"
            justifyContent="center"
            alignItems="center"
            as="button"
            onClick={() => navigate('../classroom')}
          >
            <Text>Laboratoare</Text>
          </CardBody>
        </Card>
        <Card
          flex={1}
          m="50px"
          bg={useColorModeValue('white', 'gray.700')}
          boxShadow="base"
          rounded="xl"
        >
          <CardBody
            display="flex"
            justifyContent="center"
            alignItems="center"
            as="button"
            onClick={() => navigate('../user')}
          >
            <Text>Utilizatori</Text>
          </CardBody>
        </Card>
      </Flex>
    </Screen>
  )
}