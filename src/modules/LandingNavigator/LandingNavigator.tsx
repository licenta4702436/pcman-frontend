import React from "react";
import {useCurrentUser} from "@/modules/Authentication/query/AuthenticationQuery";
import {Navigate} from "react-router-dom";


function getTo(role: 'ADMIN' | 'PROF') {
  if (role === 'ADMIN') return '/admin';

  return '/prof';
}

export type LandingNavigatorProps = unknown;

export const LandingNavigator: React.FC<LandingNavigatorProps> = () => {
  const {data: currentUser, isLoading} = useCurrentUser('dashboard', { cacheTime: 0, staleTime: 0 });

  if (currentUser == null || currentUser.role == null || isLoading) {
    return (
      <div>loading...</div>
    );
  }

  return (
    <Navigate to={getTo(currentUser.role)} replace />
  );
}