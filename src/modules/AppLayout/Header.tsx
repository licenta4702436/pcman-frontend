import React, {useCallback} from 'react';
import {
  Flex,
  FlexProps,
  Menu,
  MenuButton,
  MenuItem,
  MenuList, Spacer, Text,
  useColorModeValue,
} from '@chakra-ui/react';
import {useCurrentUser, useLogout} from "@/modules/Authentication/query/AuthenticationQuery";
import {useCurrentUserStore} from "@/modules/Authentication/UserStore";
import {useNavigate} from "react-router-dom";

export type HeaderProps = FlexProps;

export function Header({...rest}: HeaderProps) {

  const {data: currentUser} = useCurrentUser();
  const setCurrentUser = useCurrentUserStore((state) => state.setCurrentUser);
  const mutation = useLogout();

  const navigate = useNavigate();

  const handleLogout = useCallback(() => {
    mutation.mutate(undefined, {
      onSuccess: () => {
        console.log('success!');
        setCurrentUser(null);
        navigate('/login');
      },
      onError: (err) => {
        console.log(err);
      },
    });
  }, [mutation, navigate, setCurrentUser]);

  return (
    <Flex
      px={4}
      height="20"
      alignItems="center"
      bg={useColorModeValue('white', 'gray.900')}
      borderBottomWidth="1px"
      borderBottomColor={useColorModeValue('gray.200', 'gray.700')}
      justifyContent="space-between"
      {...rest}
    >
      <Spacer />
      <Menu>
        <MenuButton>
          <Text>{`${currentUser?.firstName} ${currentUser?.lastName}`}</Text>
        </MenuButton>
        <MenuList>
          <MenuItem onClick={handleLogout}>Log out</MenuItem>
        </MenuList>
      </Menu>
    </Flex>
  );
}