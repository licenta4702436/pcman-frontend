import React, {useEffect} from 'react';
import {
  Box,
} from '@chakra-ui/react';
import {Navigate, Outlet} from 'react-router-dom';
import {Header} from './Header';
import {useCurrentUser} from "@/modules/Authentication/query/AuthenticationQuery";
import {useCurrentUserStore} from "@/modules/Authentication/UserStore";
import {ErrorBox} from "@/core/components/ErrorBox";

export default function AppLayout() {
  const { data: current, isLoading, isError } = useCurrentUser();

  const currentUser = useCurrentUserStore((state) => state.currentUser);
  const setCurrentUser = useCurrentUserStore((state) => state.setCurrentUser);

  useEffect(() => {
    setCurrentUser(current);
  }, [current, setCurrentUser]);

  if (!currentUser && (!isLoading && isError)) {
    return (
      <Navigate to="/login" replace />
    )
  }

  if (isLoading) {
    return (
      <div>loading...</div>
    );
  }

  if (isError) {
    return (
      <ErrorBox />
    );
  }

  return (
    <Box minHeight="100vh" w="100vw" display="flex" flexDirection="column" bg="bg.normal">
      <Header/>
      <Box flexDirection="column" display="flex" flex={1}>
        <Outlet/>
      </Box>
    </Box>
  );
}