import React from "react";
import {Screen} from "@/core/components/Screen";
import {
  SimpleGrid,
} from "@chakra-ui/react";
import {ComputerCard} from "@/modules/Prof/components/ComputerCard";
import {useParams} from "react-router-dom";
import {useGetComputersOfClass} from "@/modules/Prof/query/ProfQuery";


export const ClassroomDashboard: React.FC = () => {
  const {classroomId} = useParams<"classroomId">();
  const {data: computers, isLoading, isError } = useGetComputersOfClass(parseInt(classroomId!));


  return (
    <Screen
      isLoading={isLoading}
      isError={isError}
    >
      <SimpleGrid
        pt={5}
        templateColumns={{base: 'repeat(3, 1fr)', md: 'repeat(4, 1fr)'}}
        gridAutoRows={{base: '150px', md: '250px'}}
        gap={4}
      >
        {!isLoading && !isError && computers.map((computer) => (
          <ComputerCard key={computer.id} computer={computer}/>
        ))}
      </SimpleGrid>

    </Screen>
  );
}