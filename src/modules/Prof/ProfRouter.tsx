import React from "react";
import {Route, Routes} from "react-router-dom";
import {Layout} from "@/core/components/Layout";
import NotFoundScreen from "@/core/components/NotFoundScreen";
import {ProfDashboard} from "@/modules/Prof/ProfDashboard";
import {ClassroomDashboard} from "@/modules/Prof/ClassroomDashboard";
import {ComputerDashboard} from "@/modules/Prof/ComputerDashboard";

export const ProfRouter: React.FC = () => {
  return (
    <Routes>
      <Route path="/" element={<Layout />} >
        <Route index element={<ProfDashboard />} />
        <Route path="/classroom/:classroomId" element={<ClassroomDashboard />} />
        <Route path="/classroom/:classroomId/computer/:computerId" element={<ComputerDashboard />} />
        <Route path="*" element={<NotFoundScreen /> } />
      </Route>
    </Routes>
  )
}