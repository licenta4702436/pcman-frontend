import React from "react";
import {SimpleGrid} from "@chakra-ui/react";
import {Screen} from "@/core/components/Screen";
import {ClassroomCard} from "@/modules/Prof/components/ClassroomCard";
import {useGetClassroomsOfCurrentProfessor} from "@/modules/Prof/query/ProfQuery";

export const ProfDashboard: React.FC = () => {
  const {data: classrooms, isLoading, isError} = useGetClassroomsOfCurrentProfessor();

  return (
    <Screen
      isLoading={isLoading}
      isError={isError}
    >
      <SimpleGrid
        pt={5}
        templateColumns={{base: 'repeat(3, 1fr)', md: 'repeat(4, 1fr)'}}
        gridAutoRows={{base: '150px', md: '250px'}}
        gap={4}
      >
        {!isLoading && !isError && classrooms.map((classroom) => (
          <ClassroomCard key={classroom.id} classroom={classroom}/>
        ))}
      </SimpleGrid>
    </Screen>
  )
}