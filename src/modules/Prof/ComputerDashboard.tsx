import React from "react";
import {useParams} from "react-router-dom";
import {Screen} from "@/core/components/Screen";
import { useGetComputer} from "@/modules/Prof/query/ProfQuery";
import {Flex, Text} from "@chakra-ui/react";
import {ComputerDirectoryDisplay} from "@/modules/Prof/components/ComputerDirectoryDisplay";
import {ComputerVideoDisplay} from "@/modules/Prof/components/ComputerVideoDisplay";


export const ComputerDashboard: React.FC = () => {
  const {computerId} = useParams<"computerId">();
  const {data: computer, isLoading, isError} = useGetComputer(parseInt(computerId!));

  return (
    <Screen
      isLoading={isLoading}
      isError={isError}
    >
      <Flex
        direction="column"
        justifyContent="center"
        alignItems="center"
        flex={1}
      >
        <Flex
          flex={1}
          width="100%"
          bg="white"
          mt={5}
          rounded="25px"
          justifyContent="center"
          alignItems="center"
          direction="column"
        >
          <Text>Video:</Text>
          <ComputerVideoDisplay computerId={parseInt(computerId!)}/>
        </Flex>
        <Flex
          flex={1}
          width="100%"
          bg="white"
          mt={5}
          rounded="25px"
          justifyContent="center"
          alignItems="center"
          direction="column"
        >
          <Text>Fisiere:</Text>
          <ComputerDirectoryDisplay computerId={parseInt(computerId!)}/>
        </Flex>
      </Flex>
    </Screen>
  )
}