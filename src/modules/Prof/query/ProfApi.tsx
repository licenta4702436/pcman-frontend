import axios from "axios";
import {Classroom} from "@/modules/Admin/Maintanence/Classroom/query/ClassroomTypes";
import {
  Computer,
  ComputerCommandMutation,
  CreateFilePayload, DeleteFilePayload
} from "@/modules/Admin/Maintanence/Computer/query/ComputerTypes";

export default ({
  getClassroomsOfCurrentProfessor: () => axios.get<Classroom[]>("/public/professor/classroom"),
  getComputersOfClass: (classroomId: number) => axios.get<Computer[]>(`/public/professor/${classroomId}/computers`),
  getComputer: (computerId: number) => axios.get<Computer>(`/public/computer/${computerId}`),
  pingComputer: (computerId: number) => axios.get(`/public/computer/${computerId}/ping`),
  executeCommandComputer: (computerId: number, payload: ComputerCommandMutation) => axios.post(`/public/computer/${computerId}/command`, payload),
  shutdownComputer: (computerId: number) => axios.post(`/public/computer/${computerId}/shutdown`),
  restartComputer: (computerId: number) => axios.post(`/public/computer/${computerId}/restart`),
  getDedicatedFileSystem: (computerId: number) => axios.get(`/public/computer/${computerId}/dedicated-file-system`),
  createFileInDedicatedFileSystem: (computerId: number, payload: CreateFilePayload) => axios.post(`/public/computer/${computerId}/dedicated-file-system`, payload),
  deleteFileInDedicatedFileSystem: (computerId: number, payload: DeleteFilePayload) => axios.post(`/public/computer/${computerId}/dedicated-file-system/delete`, payload),
  takeScreenshot: (computerId: number) => axios.post(`/public/computer/${computerId}/screenshot`, undefined, {responseType: "arraybuffer"}),
  takeWebcamPhoto: (computerId: number) => axios.post(`/public/computer/${computerId}/webcam`, undefined, {responseType: "arraybuffer"}),
})