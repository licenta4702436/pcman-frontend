import {useMutation, useQuery, useQueryClient} from "@tanstack/react-query";
import api from "./ProfApi";
import {getData, invalidateQueriesOptions} from "@/core/query/QueryUtils";
import {ComputerCommandMutation, CreateFilePayload} from "@/modules/Admin/Maintanence/Computer/query/ComputerTypes";

export function useGetClassroomsOfCurrentProfessor() {
  return useQuery(['prof', 'classrooms'], () => api.getClassroomsOfCurrentProfessor().then(getData));
}

export function useGetComputersOfClass(classroomId: number) {
  return useQuery(['prof', 'classroom', classroomId, 'computers'], () => api.getComputersOfClass(classroomId).then(getData));
}

export function useGetComputer(computerId: number) {
  return useQuery(['prof', 'computer', computerId], () => api.getComputer(computerId).then(getData));
}

export function usePingComputer(computerId: number) {
  return useQuery(['prof', 'computer', computerId, 'ping'], () => api.pingComputer(computerId).then(getData));
}

export function useExecuteCommandComputer(computerId: number) {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("prof", queryClient);
  return useMutation<string, Error, ComputerCommandMutation>((payload: ComputerCommandMutation) => api.executeCommandComputer(computerId, payload).then(getData), options);
}

export function useShutdownComputer(computerId: number) {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("prof", queryClient);
  return useMutation(() => api.shutdownComputer(computerId).then(getData), options);
}

export function useRestartComputer(computerId: number) {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("prof", queryClient);
  return useMutation(() => api.restartComputer(computerId).then(getData), options);
}

export function useDedicatedDedicatedFileSystem(computerId: number) {
  return useQuery(['computer', computerId, 'file-system'], () => api.getDedicatedFileSystem(computerId).then(getData));
}

export function useCreateFileInDedicatedFileSystem(computerId: number) {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("file-system", queryClient);
  return useMutation<void, Error, CreateFilePayload>((payload) => api.createFileInDedicatedFileSystem(computerId, payload).then(getData), options);
}

export function useDeleteFileInDedicatedFileSystem(computerId: number) {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("file-system", queryClient);
  return useMutation<void, Error, CreateFilePayload>((payload) => api.deleteFileInDedicatedFileSystem(computerId, payload).then(getData), options);
}

export function useTakeScreenshot(computerId: number) {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("computer", queryClient);
  return useMutation<any, Error, void>(() => api.takeScreenshot(computerId).then(getData), options);
}

export function useTakeWebcamPhoto(computerId: number) {
  const queryClient = useQueryClient();
  const options = invalidateQueriesOptions("computer", queryClient);
  return useMutation<any, Error, void>(() => api.takeWebcamPhoto(computerId).then(getData), options);
}
