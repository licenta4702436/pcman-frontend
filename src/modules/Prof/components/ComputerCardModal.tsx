import React, {useCallback, useRef, useState} from "react";
import {useForm} from "react-hook-form";
import {
  Box,
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent, ModalFooter,
  ModalHeader,
  ModalOverlay, Text
} from "@chakra-ui/react";
import {Computer, ComputerCommandMutation} from "@/modules/Admin/Maintanence/Computer/query/ComputerTypes";
import {useExecuteCommandComputer} from "@/modules/Prof/query/ProfQuery";
import {InputControl} from "@/core/components/Form";

export type ComputerCardModalProps = {
  computer: Computer;
  isOpen: boolean;
  onClose: any;
}

export const ComputerCardModal: React.FC<ComputerCardModalProps> = (props) => {
  const {computer, isOpen, onClose} = props;
  const {handleSubmit, control} = useForm<ComputerCommandMutation>();
  const initialRef = useRef(null);

  const commandMutation = useExecuteCommandComputer(computer.id);
  const [commandResult, setCommandResult] = useState<string | undefined>();

  const onExecute = useCallback((payload: ComputerCommandMutation) => {
    commandMutation.mutate(payload, {
      onSuccess: (result) => setCommandResult(result),
      onError: () => console.log('command error!')
    })
  }, [commandMutation]);

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      initialFocusRef={initialRef}
    >
      <ModalOverlay/>
      <ModalContent>
        <ModalHeader>Comandă</ModalHeader>
        <ModalCloseButton/>
        <ModalBody pb={6}>
          {!commandMutation.isError ? (
            <Box maxHeight="350px" overflowY="scroll">
              <Text>{commandResult}</Text>
            </Box>
          ) : (
           <Text>Eroare!</Text>
          )}
          <Box as="form" onSubmit={handleSubmit(onExecute)}>
            <InputControl
              name="command"
              label="Comandă"
              control={control}
            />
            <Button type="submit">Trimite</Button>
          </Box>
        </ModalBody>
        <ModalFooter>
          <Button onClick={onClose}>Cancel</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}