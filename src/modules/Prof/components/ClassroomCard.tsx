import React from "react";
import {Button, Card, CardBody, Flex, Text, useColorModeValue} from "@chakra-ui/react";
import {Classroom} from "@/modules/Admin/Maintanence/Classroom/query/ClassroomTypes";
import {useNavigate} from "react-router-dom";

export type ClassroomCardProps = {
  classroom: Classroom | any;
}

export const ClassroomCard: React.FC<ClassroomCardProps> = (props) => {
  const {classroom} = props;

  const navigate = useNavigate();
  return (
    <Card
      flex={1}
      bg={useColorModeValue('white', 'gray.700')}
      boxShadow="base"
      rounded="xl"
    >
      <CardBody
        display="flex"
        justifyContent="center"
        alignItems="center"
        as="button"
        onClick={() => navigate(`./classroom/${classroom.id}`)}
      >
        <Text>{classroom.name}</Text>
      </CardBody>
    </Card>
  )
}