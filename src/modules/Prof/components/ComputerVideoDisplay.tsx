import React, {useCallback, useRef} from "react";
import {Button, Flex} from "@chakra-ui/react";
import {useTakeScreenshot, useTakeWebcamPhoto} from "@/modules/Prof/query/ProfQuery";

export type ComputerVideoDisplayProps = {
  computerId: number;
};


export const ComputerVideoDisplay: React.FC<ComputerVideoDisplayProps> = (props) => {
  const {computerId} = props;

  const takeScreenshotMutation = useTakeScreenshot(computerId);
  const takeWebcamPhotoMutation = useTakeWebcamPhoto(computerId);
  const canvasRef = useRef<any>();

  const onTakeScreenshot = useCallback(() => {
    takeScreenshotMutation.mutate(undefined, {
      onSuccess: (data) => {
        const blob = new Blob([data], {type: 'image/png'});
        const imgURL = URL.createObjectURL(blob);
        const img = new Image();
        img.onload = () => {
          canvasRef.current?.getContext('2d').drawImage(img, 0, 0, 1920, 1080);
          console.log('done');
        }
        img.onerror = () => {
          console.log('img error', imgURL);
        }
        img.src = imgURL;
      },
      onError: () => {
        console.log('Error!')
      }
    });
  }, [takeScreenshotMutation, canvasRef]);

  const onTakeWebcamPhoto = useCallback(() => {
    takeWebcamPhotoMutation.mutate(undefined, {
      onSuccess: (data) => {
        const blob = new Blob([data], {type: 'image/png'});
        const imgURL = URL.createObjectURL(blob);
        const img = new Image();
        img.onload = () => {
          canvasRef.current?.getContext('2d').drawImage(img, 0, 0, 1920, 1080);
          console.log('done');
        }
        img.onerror = () => {
          console.log('img error', imgURL);
        }
        img.src = imgURL;
      },
      onError: () => {
        console.log('Error!')
      }
    });
  }, [takeWebcamPhotoMutation, canvasRef]);

  const onDownload = useCallback(() => {
    const a = document.createElement('a');
    a.href = canvasRef.current.toDataURL();
    a.download = 'image.png';
    a.click();
    a.remove();
  }, []);

  return (
    <Flex
      p={5}
      flex={1}
      width="100%"
      direction="column"
      overflowY="scroll"
    >
      <Button onClick={onTakeScreenshot}>Screenshot</Button>
      <Button onClick={onTakeWebcamPhoto}>Photo</Button>
      <canvas ref={canvasRef} width="1920px" height="1080px" />
      <Button onClick={onDownload}>Descarca poza</Button>
    </Flex>
  );
};