import React, {useCallback, useRef} from "react";
import {
  Button,
  Card,
  CardBody, CardFooter, CardHeader,
  Flex, FormControl, FormLabel,
  IconButton, Input,
  Menu,
  MenuButton,
  MenuItem,
  MenuList, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay,
  Text,
  useColorModeValue, useDisclosure
} from "@chakra-ui/react";
import {BsPower} from '@react-icons/all-files/bs/BsPower';
import {Computer} from "@/modules/Admin/Maintanence/Computer/query/ComputerTypes";
import {useNavigate} from "react-router-dom";
import {
  useExecuteCommandComputer,
  usePingComputer,
  useRestartComputer,
  useShutdownComputer
} from "@/modules/Prof/query/ProfQuery";
import {ComputerCardModal} from "@/modules/Prof/components/ComputerCardModal";

export type ComputerCardProps = {
  computer: Computer;
}
export const ComputerCard: React.FC<ComputerCardProps> = (props) => {
  const {computer} = props;
  const navigate = useNavigate();

  const {data: computerStatus, isLoading, isError} = usePingComputer(computer.id);

  const shutdownMutation = useShutdownComputer(computer.id);
  const restartMutation = useRestartComputer(computer.id);


  const onShutdown = useCallback(() => {
    shutdownMutation.mutate(undefined, {
      onSuccess: () => console.log('shutdown success!'),
      onError: () => console.log('shutdown error!')
    })
  }, [shutdownMutation]);

  const onRestart = useCallback(() => {
    restartMutation.mutate(undefined, {
      onSuccess: () => console.log('restart success!'),
      onError: () => console.log('restart error!')
    })
  }, [restartMutation]);

  const {isOpen, onOpen, onClose} = useDisclosure();

  return (
    <Card
      flex={1}
      bg={useColorModeValue('white', 'gray.700')}
      boxShadow="base"
      rounded="xl"
    >
      <CardHeader
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        py="-10px"
      >
        <Text display="block" as="b" fontSize="md">{computer.name}</Text>
        <Flex justifyContent="space-between" w="100%">
          <Text display="block" as="b" fontSize="md">Status: {!isLoading ? computerStatus : (isError ? 'err' : '...')} </Text>
          <Text display="block" as="b" fontSize="md">Ip: {computer.ipAddress} </Text>
        </Flex>
      </CardHeader>
      <CardBody
        display="flex"
        justifyContent="center"
        alignItems="center"
        as="button"
        onClick={() => navigate(`./computer/${computer.id}`)}
      >
        <p>dada</p>
      </CardBody>
      <CardFooter
        display="flex"
        justifyContent="flex-end"
      >
        <Menu>
          <MenuButton
            as={IconButton}
            icon={<BsPower/>}
          />
          <MenuList>
            <MenuItem onClick={onOpen}>Execută comandă</MenuItem>
            <MenuItem onClick={onRestart}>Repornește</MenuItem>
            <MenuItem onClick={onShutdown}>Închide</MenuItem>
          </MenuList>
        </Menu>
      </CardFooter>
      <ComputerCardModal computer={computer} isOpen={isOpen} onClose={onClose} />
    </Card>
  )
}