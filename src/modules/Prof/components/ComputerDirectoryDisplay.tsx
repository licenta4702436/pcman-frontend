import React, {useCallback, useEffect, useState} from "react";
import {Box, Button, Flex, Input, Text} from "@chakra-ui/react";
import {
  useCreateFileInDedicatedFileSystem,
  useDedicatedDedicatedFileSystem,
  useDeleteFileInDedicatedFileSystem
} from "@/modules/Prof/query/ProfQuery";


export type ComputerDirectoryDisplayProps = {
  computerId: number;
};

const getFilesToDisplay = (fileSystem: any, currentPath: string[]) => {
  let filesToDisplay = fileSystem.children;
  for (const path of currentPath) {
    filesToDisplay = filesToDisplay.find(file => file.name === path).children;
  }
  return filesToDisplay;
}

const getPathToCurrentFile = (fileSystem: any, currentPath: string[]) => {
  let currentFile = fileSystem;
  for (const path of currentPath) {
    currentFile = currentFile.children.find(file => file.name === path);
  }

  return currentFile.path;
}


export const ComputerDirectoryDisplay: React.FC<ComputerDirectoryDisplayProps> = (props) => {
  const {computerId} = props;
  const {data: fileSystem, isLoading, isError} = useDedicatedDedicatedFileSystem(computerId);
  const createFileMutation = useCreateFileInDedicatedFileSystem(computerId);
  const deleteFileMutation = useDeleteFileInDedicatedFileSystem(computerId);

  const [currentPath, setCurrentPath] = useState<string[]>([]);

  const [newFileName, setNewFileName] = useState<string>('');

  const traverseIn = useCallback((currentFileSystem: any) => {
    if (currentFileSystem.isDirectory) {
      setCurrentPath(prevState => [...prevState, currentFileSystem.name]);
    }
  }, []);

  const traverseOut = useCallback(() => {
    setCurrentPath(prevState => [...prevState.slice(0, -1)]);
  }, []);

  const onCreateFile = useCallback(() => {
    createFileMutation.mutate({path: getPathToCurrentFile(fileSystem, currentPath) + `/${newFileName ?? 'default_name.txt'}`}, {
      onSuccess: () => {
        console.log('Success!')
      },
      onError: () => {
        console.log('Error!')
      }
    })
  }, [currentPath, fileSystem, createFileMutation, newFileName])
  
  const onDeleteFile = useCallback((file: any) => {
    deleteFileMutation.mutate({path: file.path}, {
      onSuccess: () => {
        console.log('Success!')
      },
      onError: () => {
        console.log('Error!')
      }
    })
  }, [deleteFileMutation]);

  return (
    <Flex
      p={5}
      flex={1}
      width="100%"
      direction="column"
      overflowY="scroll"
    >
      <Flex
        justifyContent="space-between"
        py={5}>
        <Button onClick={traverseOut}>Inapoi</Button>
        <Flex>
          <Input value={newFileName} placeholder="Nume fisier" onChange={(e) => setNewFileName(e.target.value)} mr={5}/>
          <Button disabled={createFileMutation.isLoading || createFileMutation.isError} onClick={onCreateFile}>Creeaza fisier</Button>
        </Flex>
      </Flex>
      {!isLoading && !isError && getFilesToDisplay(fileSystem, currentPath).map(file =>
        (
          <Flex
            key={file.name}
          >
            <Button
              onClick={() => traverseIn(file)}
              width={{base: 'auto', md: '25%'}}
              m={1}
            >
              {file.name}
            </Button>
            <Button
              onClick={() => onDeleteFile(file)}
              width={{base: '25%', md: '10%'}}
              m={1}
            >
              Sterge
            </Button>
          </Flex>
        )
      )}
    </Flex>
  )
}