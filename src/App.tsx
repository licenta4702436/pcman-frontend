import React, {Suspense} from 'react'
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import {ChakraProvider} from "@chakra-ui/react";
import {Navigator} from "@/modules/Navigator";
import {PageLoading} from "@/core/components/PageLoading";

import chakraTheme from "@/core/chakraTheme";
import './core/axiosInitialize';
import './App.css'

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false,
            retry: false,
        },
    },
});

function App() {
  return (
      <QueryClientProvider client={queryClient}>
          <ChakraProvider theme={chakraTheme}>
              <div className="App">
                  <Suspense fallback={<PageLoading />}>
                      <Navigator />
                  </Suspense>
              </div>
          </ChakraProvider>
      </QueryClientProvider>
  );
}

export default App
