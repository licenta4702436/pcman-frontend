import axios from 'axios';
import qs from 'qs';

axios.defaults.baseURL = '/api';
axios.defaults.paramsSerializer = {
    serialize: function paramsSerializer(params: any) {
        return qs.stringify(params, { arrayFormat: 'indices', allowDots: true, skipNulls: true });
    },
};

axios.interceptors.request.use(
  (request) => {
      const token = localStorage.getItem("authToken");
      if (token) {
          request.headers.Authorization = token;
      }
      return request;
  }
)

axios.interceptors.response.use(
    (response) => response,
    (error) => Promise.reject(error),
);