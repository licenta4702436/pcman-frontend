import { Box, Heading, Text, Button } from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';

export default function NotFoundScreen() {

  return (
    <Box mt={40} maxHeight="300px" bg="white" border={12} textAlign="center" py={10} px={6}>
      <Heading
        display="inline-block"
        as="h2"
        size="2xl"
        bgGradient="linear(to-r, teal.400, teal.600)"
        backgroundClip="text"
      >
        404
      </Heading>
      <Text fontSize="18px" mt={3} mb={2}>
        :(
      </Text>
      <Text color="gray.500" mb={6}>
        Pagina nu există
      </Text>

      <Button
        as={RouterLink}
        colorScheme="teal"
        bgGradient="linear(to-r, teal.400, teal.500, teal.600)"
        color="white"
        variant="solid"
        to="/"
      >
        Acasă
      </Button>
    </Box>
  );
}