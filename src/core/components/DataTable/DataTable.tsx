import React, {useCallback, useState} from "react";
import {
  Box,
  Button,
  Table,
  TableContainer,
  TableProps,
  Text,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  IconButton
} from "@chakra-ui/react";
import {useNavigate} from "react-router-dom";
import {Screen} from "@/core/components/Screen";
import {DeleteIcon} from "@chakra-ui/icons";


export type TableColumnConfig = {
  key: string;
  name: string;
  clickable?: boolean;
};

export type TableConfig = {
  columns: TableColumnConfig[];
}


export type DataTableProps = Partial<Omit<TableProps, 'onChange'>> & {
  config: TableConfig;

  useGetData: any;
  useDeleteData: any;
  dependencies?: any[];

  extendUrl?: boolean;
}

export const DataTable: React.FC<DataTableProps> = (props) => {
  const {config, useGetData, useDeleteData, dependencies, extendUrl} = props;

  const navigate = useNavigate();
  const onAdd = useCallback(() => navigate(`${extendUrl ? './' : ''}../create`), [extendUrl, navigate]);

  const deleteMutation = useDeleteData();
  const onDelete = useCallback((id: number) => {
    deleteMutation.mutate(id, {
      onSuccess: () => console.log("delete success!"),
      onError: () => console.log("delete error!")
    })
  }, [deleteMutation]);

  const {data, isLoading, isError} = useGetData(dependencies ? [...dependencies] : undefined);

  return (
    <Screen isLoading={isLoading} isError={isError}>
      <Box mb={4} display="flex" justifyContent="space-between" alignItems="center">
        <Button colorScheme="cyan" onClick={() => navigate('../..')}>
          Înapoi
        </Button>
        <Button colorScheme="cyan" onClick={onAdd}>
          Creează
        </Button>
      </Box>
      <TableContainer minHeight="15vh" maxHeight="70vh" minWidth="80%" mb="15" shadow="base" bg="white"
                      overflowY="auto">
        <Table variant="simple">
          <Thead position="sticky" top={0} bg="cyan" zIndex={2}>
            <Tr>
              {config.columns.map(el => (
                <Th key={el.key}>
                  {el.name}
                </Th>
              ))}
              <Th key="delete">
                Șterge
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {!isError && data?.map(el => (
              <Tr key={el.id}>
                {config.columns.map(col => {
                  if (col.clickable) {
                    return (
                      <Td key={col.key}>
                        <Button variant="link" onClick={() => navigate(`${extendUrl ? './' : ''}../${el.id}/edit`)}>
                          {el[col.key]}
                        </Button>
                      </Td>
                    )
                  }
                  if (col.key === 'status') {
                    return (
                      <Td key={col.key}>
                        {el[col.key].value}
                      </Td>
                    )
                  }
                  return (
                    <Td key={col.key}>
                      {el[col.key]}
                    </Td>
                  )
                })}
                <Td key="delete">
                  <IconButton
                    size="sm"
                    color="white"
                    bg="red"
                    aria-label="delete icon"
                    icon={<DeleteIcon/>}
                    onClick={() => onDelete(el.id)}
                  />
                </Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Screen>
  );
}
