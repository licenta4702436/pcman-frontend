import React, {JSX} from 'react';
import { MdErrorOutline } from '@react-icons/all-files/md/MdErrorOutline';
import { Box, Heading, Text } from '@chakra-ui/react';

export function ErrorBox(): JSX.Element {

  return (
    <Box maxHeight="300px" bg="white" border={12} textAlign="center" py={10} px={6}>
      <Heading
        display="inline-block"
        as="h2"
        size="2xl"
        bgGradient="linear(to-r, teal.400, teal.600)"
        backgroundClip="text"
      >
        <MdErrorOutline color="red" size={50}/>
      </Heading>
      <Text fontSize="18px" mt={3} mb={2}>
        Eroare
      </Text>
      <Text color="gray.500" mb={6}>
        Contactati un administrator
      </Text>
    </Box>
  );
}