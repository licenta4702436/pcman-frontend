import React from "react";
import {FormControl, FormLabel} from "@chakra-ui/react";
import {Control, useController} from "react-hook-form";
import {FormControlProps} from "@chakra-ui/form-control";
import {Select, Props} from "chakra-react-select";

export type SelectControlProps = FormControlProps & {
  isRequired?: boolean;

  name: string;
  control: Control<any>;
  defaultValue?: any;

  label: string;
  type?: string;

  options: any[];
  select?: Props;

  disabled?: boolean;
  isError?: boolean;
};

export const SelectControl: React.FC<SelectControlProps> = (props) => {
  const {
    name,
    disabled,
    isError,
    control,
    defaultValue,
    label,
    isRequired,
    options,
    select,
    ...restProps
  } = props;
  const {field: {value, ref, onChange}, fieldState: {error}} = useController({
    name,
    control,
    defaultValue,
    rules: {required: isRequired}
  });

  return (
    <FormControl isRequired={isRequired} isInvalid={!!error?.message || isError} {...restProps}>
      <FormLabel>{label}</FormLabel>
      <Select {...select} isDisabled={disabled} options={options} value={value} ref={ref} onChange={onChange} />
    </FormControl>
  );
}