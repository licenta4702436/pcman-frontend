import React from 'react';
import { FormControl, FormErrorMessage, FormLabel, Input } from '@chakra-ui/react';
import { FormControlProps } from '@chakra-ui/form-control';
import { Control, useController } from 'react-hook-form';
import {TooltipIcon} from "@/core/components/TooltipIcon";

export type InputControlProps = FormControlProps & {
  isRequired?: boolean;

  name: string;
  control: Control<any>;
  defaultValue?: any;

  label?: string;
  type?: string;

  disabled?: boolean;

  tooltip?: string | null;
};

export const InputControl: React.FC<InputControlProps> = (props) => {
  const { name, control, defaultValue, label, type, disabled, isRequired, tooltip, ...restProps } = props;
  const { field: { value, ref, onChange }, fieldState: { error } } = useController({ name, control, defaultValue: defaultValue ?? '' });

  return (
    <FormControl isRequired={isRequired} isInvalid={!!error?.message} {...restProps}>
      {label && (
        <FormLabel>
          {label}
          {' '}
          {tooltip && <TooltipIcon label={tooltip} />}
        </FormLabel>
      )}
      <Input disabled={disabled} type={type} value={value} ref={ref} onChange={onChange} />
      {error?.message && <FormErrorMessage>{error?.message}</FormErrorMessage>}
    </FormControl>
  );
};