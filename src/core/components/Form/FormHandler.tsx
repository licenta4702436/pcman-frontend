import React from 'react';
import { Box, Button, Center, Flex, Spinner, Text } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';

export type FormHandlerProps = {
  onSubmit: any;
  children: any;
  title: string;
  isLoading?: boolean;
  confirmText?: string;
  discardText?: string;
};

export const FormHandler: React.FC<FormHandlerProps> = ({ children, onSubmit, title, isLoading, confirmText, discardText}) => {
  const navigate = useNavigate();
  return (
    <Flex
      flexDirection="column"
      justifyContent="space-between"
      boxShadow="base"
      position="relative"
      rounded="md"
      flex={1}
      m="5%"
      pl="40px"
      pr="40px"
      pt="20px"
      pb="20px"
      width="90%"
      height="100%"
      bg="white"
      as="form"
      onSubmit={onSubmit}
    >
      <Box>
        <Text display="block" mb="15px" as="b" fontSize="xl">{title}</Text>
        {children}
      </Box>
      <Center>
        <Button mr={10} color="mainButton" type="submit">{confirmText}</Button>
        <Button bg="secondaryButton" onClick={() => navigate('..')}>{discardText}</Button>
      </Center>
      {isLoading && (
        <Flex
          justifyContent="center"
          bg="rgba(0, 0, 0, 0.11)"
          borderRadius={8}
          alignItems="center"
          position="absolute"
          top={0}
          left={0}
          right={0}
          bottom={0}
        >
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="teal.500"
            size="xl"
          />
        </Flex>
      )}
    </Flex>
  );
};