import {PropsWithChildren} from "react";
import {useCurrentUser} from "@/modules/Authentication/UserStore";
import NotFoundScreen from "@/core/components/NotFoundScreen";

type RoleBoundaryProps = {
  permittedRoles: string[];
}

export const RoleBoundary = (props: PropsWithChildren<RoleBoundaryProps>) => {
  const {permittedRoles, children} = props;
  const currentUser = useCurrentUser();

  if (currentUser && permittedRoles.indexOf(currentUser?.role) === -1) {
    return <NotFoundScreen />;
  }

  return (
    <>
      {children}
    </>
  )
};