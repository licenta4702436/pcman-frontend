import React, { ReactNode } from 'react';
import { Box, Flex, Spinner } from '@chakra-ui/react';
import { ErrorBox } from '@/core/components/ErrorBox';

export type ScreenProps = {
  isLoading?: boolean;
  isError?: boolean;
  children: ReactNode;
  p?: string;
};

export const Screen: React.FC<ScreenProps> = (props) => {
  const { isLoading, isError, children, ...rest } = props;
  return (
    <Box p="5%" display="flex" flex={1} width="100%" flexDirection="column" {...rest}>
      {isLoading && !isError && (
        <Flex flex={1} justifyContent="center" alignItems="center">
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="bg.normal"
            size="xl"
          />
        </Flex>
      )}
      {isError && (
        <Flex flex={1} justifyContent="center" alignItems="center">
          <ErrorBox />
        </Flex>
      )}
      {!isLoading && !isError && children}
    </Box>
  );
};