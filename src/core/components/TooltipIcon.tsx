import React from 'react';
import { BsFillInfoCircleFill } from '@react-icons/all-files/bs/BsFillInfoCircleFill';
import { Icon, Tooltip } from '@chakra-ui/react';

export type TooltipProps = {
  label: string;
};

export const TooltipIcon = ({ label }: TooltipProps) => (
  <Tooltip label={label}>
    <span>
      <Icon as={BsFillInfoCircleFill} color="#4392aa" />
    </span>
  </Tooltip>
);