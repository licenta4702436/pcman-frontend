import React from 'react';
import { Outlet } from 'react-router-dom';
import { Box } from '@chakra-ui/react';

export type LayoutProps = {};

export const Layout: React.FC<LayoutProps> = () => (
  <Box flex={1} display="flex" alignItems="center" flexDirection="column">
    <Outlet />
  </Box>
);