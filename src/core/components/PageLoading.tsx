import React from 'react';
import { Flex, Spinner } from '@chakra-ui/react';

export type PageLoadingProps = {

};

export const PageLoading: React.FC<PageLoadingProps> = () => (
  <Flex
    justifyContent="center"
    bg="rgba(0, 0, 0, 0.11)"
    borderRadius={8}
    alignItems="center"
    position="absolute"
    top={0}
    left={0}
    right={0}
    bottom={0}
  >
    <Spinner
      thickness="4px"
      speed="0.65s"
      emptyColor="gray.200"
      color="teal.500"
      size="xl"
    />
  </Flex>
);
