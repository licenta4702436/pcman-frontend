import { extendTheme } from '@chakra-ui/react';

const chakraTheme = extendTheme({
    colors: {
        bg: {
            pale: "#EDFDFD",
            normal: "#C4F1F9",
            dark: "#9DECF9",
        },
        mainButton: "#2b6cb0",
        mainButtonHover: "#2c5282",
        secondaryButton: "",
        secondaryButtonHover: "",
    },

});

export default chakraTheme;