import {AxiosResponse} from "axios";
import {QueryClient} from "@tanstack/react-query";

export function getData<T>(response: AxiosResponse<T>) {
  return response.data;
}

export function getResponse<T>(response: AxiosResponse<T>) {
  return response;
}

export function invalidateQueriesOptions(key: any, queryClient: QueryClient) {
  return ({
    onSuccess: async () => await queryClient.invalidateQueries({ queryKey: key })
  })
}